#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

  double tf=3;
  double dt=0.001;

 
  Eigen::Vector2d XF(0,0.6);
  Eigen::Vector2d xd(0.0 , 0.6);
  Eigen::Vector2d Dxd_ff;
  Eigen::Vector2d X_d;
  Eigen::Vector2d dX_d;
  Eigen::Vector2d X;
  Eigen::Matrix2d J;
  Eigen::Vector2d q(M_PI_2,M_PI_4);
  Eigen::Vector2d  qddot;
  Eigen::Vector2d Xa;

  RobotModel robot(0.4,0.5);
 
  Controller controller(robot);

  robot.FwdKin(X,J,q);

  Point2Point traj(X, XF, tf);
  
  std::cout << "t,X,Y,X_d,Y_d"<<std::endl;
  
  for (double  i = 0 ; i < tf ; i=i+dt ){

    X_d=traj.X(i);
    dX_d=traj.dX(i);


    qddot= controller.Dqd(q, X_d, dX_d);

    q=q+qddot*dt;

    robot.FwdKin(Xa,J,q);
  
    std::cout << std::endl << i <<","<< Xa(0) <<","<< Xa(1) <<","<< X_d(0) <<","<< X_d(1)<< std::endl;

  
  }
}
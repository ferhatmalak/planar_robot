#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //initialize the object polynomial coefficients

  pi= piIn;
  pf= pfIn;
  Dt= DtIn;

};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  // update polynomial coefficients
 pi= piIn;
  pf= pfIn;
  Dt= DtIn;

  a[0]=0;
  a[1]=0;
  a[2]=0;
  a[3]=10;
  a[4]=-15;
  a[5]=6;


};

const double  Polynomial::p     (const double &t){
  //Tcalculat the position

    double pst{0};

    update(pi, pf, Dt);

    pst= pi+ (a[0]+ a[1]*(t/Dt)+ a[2]*pow(t/Dt,2)+ a[3]* pow(t/Dt,3)+ a[4]* pow(t/Dt,4)+ a[5]* pow(t/Dt,5))*(pf-pi);

  return pst;
};

const double  Polynomial::dp    (const double &t){
  //calculat the velocity
    double vts{0};

    vts= -(a[1]+ 2*a[2]*(t/Dt)+ 3*a[3]*pow(t/Dt,2)+ 4*a[4]*pow(t/Dt,3)+ 5*a[5]*pow(t/Dt,4))*(pf-pi);


  return vts;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  // initialize object and polynomials0

    polx.update(xi(0), xf(0), DtIn);
    poly.update(xi(1), xf(1), DtIn);
    Dt= DtIn;


}

Eigen::Vector2d Point2Point::X(const double & time){
  // compute cartesian position

    Eigen::Vector2d Xd(0,0);

    Xd(0)= polx.p(time);
    Xd(1)= poly.p(time);


  return Xd;
}

Eigen::Vector2d Point2Point::dX(const double & time){

  //compute cartesian velocity

  Eigen::Vector2d Vd;

    Vd(0)=polx.dp(time);
    Vd(1)=poly.dp(time);

  return Vd;
}